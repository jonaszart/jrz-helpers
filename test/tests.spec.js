const assert = require('assert');
const helpers = require('../index');

describe('', () => {
	it('randomNumber', () => {
		const gen = helpers.randomNumber(1, 999999999);
		console.log(gen);
	});
	it('randomEmail', () => {
		const gen = helpers.randomEmail();
		console.log(gen);
	});
	it('randomPhone', () => {
		const gen = helpers.randomPhone();
		console.log(gen);
	});
	it('jpgBase64', () => {
		const gen = helpers.jpgBase64();
		console.log(`${gen.slice(0, 20)}...`);
	});
	it('pngBase64', () => {
		const gen = helpers.pngBase64();
		console.log(`${gen.slice(0, 20)}...`);
	});
	it('pdfBase64', () => {
		const gen = helpers.pdfBase64();
		console.log(`${gen.slice(0, 20)}...`);
	});
	it('getBase64Extension (JPG)', () => {
		const base64 = helpers.jpgBase64();
		const gen = helpers.getBase64Extension(base64);
		console.log(gen);
	});
	it('getBase64Extension (PNG)', () => {
		const base64 = helpers.pngBase64();
		const gen = helpers.getBase64Extension(base64);
		console.log(gen);
	});
	it('getBase64Extension (PDF)', () => {
		const base64 = helpers.pdfBase64();
		const gen = helpers.getBase64Extension(base64);
		console.log(gen);
	});
	it('randomProductName', () => {
		const gen = helpers.randomProductName();
		console.log(gen);
	});
	it('randomName', () => {
		const gen = helpers.randomName();
		console.log(gen);
	});
	it('randomStringId', () => {
		const gen = helpers.randomStringId();
		console.log(gen);
	});
	it('randomCnpj', () => {
		const gen = helpers.randomCnpj();
		console.log(gen);
	});
	it('randomUuid', () => {
		const gen = helpers.randomUuid();
		console.log(gen);
	});
	it('randomCpf', () => {
		const gen = helpers.randomCpf();
		console.log(gen);
	});
	it('insertBase64DataContent (JPG)', () => {
		const base64 = helpers.jpgBase64();
		const gen = helpers.insertBase64DataContent(base64);
		console.log(`${gen.slice(0, 40)}...`);
	});
	it('insertBase64DataContent (PNG)', () => {
		const base64 = helpers.pngBase64();
		const gen = helpers.insertBase64DataContent(base64);
		console.log(`${gen.slice(0, 40)}...`);
	});
	it('insertBase64DataContent (PDF)', () => {
		const base64 = helpers.pdfBase64();
		const gen = helpers.insertBase64DataContent(base64);
		console.log(`${gen.slice(0, 40)}...`);
	});
	it('getBase64Value', () => {
		const base64 = helpers.jpgBase64DataContent();
		const gen = helpers.getBase64Value(base64);
		console.log(`${gen.slice(0, 40)}...`);
		const gen2 = helpers.getBase64Extension(gen);
		assert.equal(gen2, 'jpg');
	});
	it('getBase64Value when its already base64 value', () => {
		const base64 = helpers.jpgBase64();
		const gen = helpers.getBase64Value(base64);
		const gen2 = helpers.getBase64Value(gen);
		console.log(`${gen2.slice(0, 40)}...`);
		const gen3 = helpers.getBase64Extension(gen2);
		assert.equal(gen3, 'jpg');
	});
	it('slugify', () => {
		const str = 'Um Produtão 432 Mútc~^o';
		const gen = helpers.slugify(str);
		console.log(gen);
	});
	it('randomDescription', () => {
		const gen = helpers.randomDescription();
		console.log(gen);
	});
	it('random base64 image', () => {
		const gen = helpers.randomBase64Image();
		console.log(`${gen.slice(0, 60)}...`);
	});
});
