const fs = require('fs');
const randomNumber = require('./randomNumber');

module.exports = () => {
	const rootDir = `${__dirname}/images/`;
	const files = fs.readdirSync(rootDir);
  const bitmap = fs
  	.readFileSync(`${rootDir}${files[randomNumber(0, files.length - 1)]}`);
  return Buffer.from(bitmap).toString('base64');
};
